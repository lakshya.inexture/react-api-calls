import React, { useState, useEffect } from "react";
import axios from "axios";
import Axios from "./components/Axios";
import Fetch from "./components/Fetch";
import NewsApi from "./components/NewsApi";

function App() {
    const [post, setPost] = useState(null);
    const url = "https://jsonplaceholder.typicode.com/posts";

    useEffect(() => {
        axios.get(url).then((response) => {
            setPost(response.data);
        });
    }, []);
    if (!post) return "No post";
    // Above method uses axios to GET data

    const createData = () => {
        axios
            .post(url, {
                title: "Lakshya",
                body: "Ahmedabad",
            })
            .then((response) => setPost(response.data));
    };
    // Above method adds data using POST

    const deleteData = () => {
        axios.delete(`${url}/1`).then(() => setPost(""));
    };

    // useEffect(() => {
    //     fetch(url)
    //         .then((response) => response.json())
    //         .then((data) => {
    //             setPost(data);
    //             // console.log(data);
    //         });
    // });
    // if (!post) return null;
    // Above method is simple fetch method

    return (
        <div>
            {/* <h1>{post.title}</h1>
            <p>{post.body}</p>
            <button onClick={createData}>Create Data</button>
            <button onClick={deleteData}>Delete Data</button> */}
            {/* <Fetch
                createData={createData}
                deleteData={deleteData}
                response={post}
            /> */}
            {/* Prints POST data, now moved to Fetch component */}
            {/* {post.map((res, ind) => (
                <Axios response={res} key={ind} />
            ))} */}
            {/* {post.map((res) => (
                <div>
                    <h1>{res.title}</h1>
                    <p>{res.body}</p>
                </div>
            ))} */}
            {/* Prints GET data, now moved to the Axios component */}
            <NewsApi />
            {/* Entire logic of this component is in the NewsApi component. */}
        </div>
    );
}

export default App;
