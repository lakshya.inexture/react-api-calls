import React from "react";

const Axios = ({ response }) => {
    return (
        <div>
            <h1>{response.title}</h1>
            <p>{response.body}</p>
        </div>
    );
};

export default Axios;
