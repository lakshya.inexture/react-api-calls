import React, { useState, useEffect } from "react";
import axios from "axios";

const NewsApi = () => {
    const [news, setNews] = useState(null);
    const newsUrl =
        "https://newsapi.org/v2/top-headlines?country=in&apiKey=008a12d4db93457793b438997dcf3934";

    useEffect(() => {
        axios.get(newsUrl).then((res) => setNews(res.data.articles));
    }, []);
    // GET method to get all the articles from the API
    if (!news) return "No post";
    // If there is no return from the API, displays no post
    return (
        <div>
            {news.map((el, ind) => {
                return (
                    <div key={ind}>
                        <h1>{el.title}</h1>
                        <p>{el.description}</p>
                        <p>{el.content}</p>
                    </div>
                );
            })}
            {/* Prints only the title of the articles. */}
        </div>
    );
};

export default NewsApi;
