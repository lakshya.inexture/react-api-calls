import React from "react";

const Fetch = ({ response, createData, deleteData }) => {
    return (
        <div>
            <h1>{response.title}</h1>
            <p>{response.body}</p>
            <button onClick={createData}>Create Data</button>
            <button onClick={deleteData}>Delete Data</button>
        </div>
    );
};

export default Fetch;
